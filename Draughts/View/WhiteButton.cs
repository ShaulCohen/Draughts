﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draughts.View
{
    public class WhiteButton : Button
    {
        public WhiteButton()
        {
            BackColor = Color.FromArgb(255, 206, 158);
            Width = 100;
            Height = 100;
            Dock = DockStyle.Left;
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);
            pevent.Graphics.FillRectangle(new SolidBrush(BackColor), 0, 0,Width, Height);
        }

    }
}
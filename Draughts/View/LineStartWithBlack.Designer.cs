﻿namespace Draughts
{
    partial class LineStartWithBlack
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.whiteButton1 = new Draughts.View.WhiteButton();
            this.blackButton1 = new Draughts.View.BlackButton();
            this.blackButton2 = new Draughts.View.BlackButton();
            this.whiteButton2 = new Draughts.View.WhiteButton();
            this.blackButton3 = new Draughts.View.BlackButton();
            this.whiteButton3 = new Draughts.View.WhiteButton();
            this.blackButton4 = new Draughts.View.BlackButton();
            this.whiteButton4 = new Draughts.View.WhiteButton();
            this.SuspendLayout();
            // 
            // whiteButton1
            // 
            this.whiteButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(206)))), ((int)(((byte)(158)))));
            this.whiteButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.whiteButton1.Location = new System.Drawing.Point(100, 0);
            this.whiteButton1.Name = "whiteButton1";
            this.whiteButton1.Size = new System.Drawing.Size(100, 100);
            this.whiteButton1.TabIndex = 1;
            this.whiteButton1.Text = "whiteButton1";
            this.whiteButton1.UseVisualStyleBackColor = false;
            // 
            // blackButton1
            // 
            this.blackButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(139)))), ((int)(((byte)(71)))));
            this.blackButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.blackButton1.Location = new System.Drawing.Point(0, 0);
            this.blackButton1.Name = "blackButton1";
            this.blackButton1.Size = new System.Drawing.Size(100, 100);
            this.blackButton1.TabIndex = 0;
            this.blackButton1.Text = "blackButton1";
            this.blackButton1.UseVisualStyleBackColor = false;
            // 
            // blackButton2
            // 
            this.blackButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(139)))), ((int)(((byte)(71)))));
            this.blackButton2.Dock = System.Windows.Forms.DockStyle.Left;
            this.blackButton2.Location = new System.Drawing.Point(200, 0);
            this.blackButton2.Name = "blackButton2";
            this.blackButton2.Size = new System.Drawing.Size(100, 100);
            this.blackButton2.TabIndex = 2;
            this.blackButton2.Text = "blackButton2";
            this.blackButton2.UseVisualStyleBackColor = false;
            // 
            // whiteButton2
            // 
            this.whiteButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(206)))), ((int)(((byte)(158)))));
            this.whiteButton2.Dock = System.Windows.Forms.DockStyle.Left;
            this.whiteButton2.Location = new System.Drawing.Point(300, 0);
            this.whiteButton2.Name = "whiteButton2";
            this.whiteButton2.Size = new System.Drawing.Size(100, 100);
            this.whiteButton2.TabIndex = 3;
            this.whiteButton2.Text = "whiteButton2";
            this.whiteButton2.UseVisualStyleBackColor = false;
            // 
            // blackButton3
            // 
            this.blackButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(139)))), ((int)(((byte)(71)))));
            this.blackButton3.Dock = System.Windows.Forms.DockStyle.Left;
            this.blackButton3.Location = new System.Drawing.Point(400, 0);
            this.blackButton3.Name = "blackButton3";
            this.blackButton3.Size = new System.Drawing.Size(100, 100);
            this.blackButton3.TabIndex = 4;
            this.blackButton3.Text = "blackButton3";
            this.blackButton3.UseVisualStyleBackColor = false;
            // 
            // whiteButton3
            // 
            this.whiteButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(206)))), ((int)(((byte)(158)))));
            this.whiteButton3.Dock = System.Windows.Forms.DockStyle.Left;
            this.whiteButton3.Location = new System.Drawing.Point(500, 0);
            this.whiteButton3.Name = "whiteButton3";
            this.whiteButton3.Size = new System.Drawing.Size(100, 100);
            this.whiteButton3.TabIndex = 5;
            this.whiteButton3.Text = "whiteButton3";
            this.whiteButton3.UseVisualStyleBackColor = false;
            // 
            // blackButton4
            // 
            this.blackButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(139)))), ((int)(((byte)(71)))));
            this.blackButton4.Dock = System.Windows.Forms.DockStyle.Left;
            this.blackButton4.Location = new System.Drawing.Point(600, 0);
            this.blackButton4.Name = "blackButton4";
            this.blackButton4.Size = new System.Drawing.Size(100, 100);
            this.blackButton4.TabIndex = 6;
            this.blackButton4.Text = "blackButton4";
            this.blackButton4.UseVisualStyleBackColor = false;
            // 
            // whiteButton4
            // 
            this.whiteButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(206)))), ((int)(((byte)(158)))));
            this.whiteButton4.Dock = System.Windows.Forms.DockStyle.Left;
            this.whiteButton4.Location = new System.Drawing.Point(700, 0);
            this.whiteButton4.Name = "whiteButton4";
            this.whiteButton4.Size = new System.Drawing.Size(100, 100);
            this.whiteButton4.TabIndex = 7;
            this.whiteButton4.Text = "whiteButton4";
            this.whiteButton4.UseVisualStyleBackColor = false;
            // 
            // LineStartWithBlack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.whiteButton4);
            this.Controls.Add(this.blackButton4);
            this.Controls.Add(this.whiteButton3);
            this.Controls.Add(this.blackButton3);
            this.Controls.Add(this.whiteButton2);
            this.Controls.Add(this.blackButton2);
            this.Controls.Add(this.whiteButton1);
            this.Controls.Add(this.blackButton1);
            this.Name = "LineStartWithBlack";
            this.Size = new System.Drawing.Size(800, 100);
            this.ResumeLayout(false);

        }

        #endregion

        private View.BlackButton blackButton1;
        private View.WhiteButton whiteButton1;
        private View.BlackButton blackButton2;
        private View.WhiteButton whiteButton2;
        private View.BlackButton blackButton3;
        private View.WhiteButton whiteButton3;
        private View.BlackButton blackButton4;
        private View.WhiteButton whiteButton4;
    }
}

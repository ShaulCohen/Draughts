﻿namespace Draughts
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lineStartWithWhite4 = new Draughts.LineStartWithWhite();
            this.lineStartWithBlack4 = new Draughts.LineStartWithBlack();
            this.lineStartWithWhite3 = new Draughts.LineStartWithWhite();
            this.lineStartWithBlack3 = new Draughts.LineStartWithBlack();
            this.lineStartWithWhite2 = new Draughts.LineStartWithWhite();
            this.lineStartWithBlack2 = new Draughts.LineStartWithBlack();
            this.lineStartWithWhite1 = new Draughts.LineStartWithWhite();
            this.lineStartWithBlack1 = new Draughts.LineStartWithBlack();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(159)))), ((int)(((byte)(201)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 800);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 127);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(248, 225);
            this.panel5.TabIndex = 4;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(83)))), ((int)(((byte)(33)))));
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(248, 225);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(83)))), ((int)(((byte)(33)))));
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 352);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(248, 346);
            this.panel3.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(248, 346);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Blue;
            this.panel6.Controls.Add(this.button1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 698);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(248, 100);
            this.panel6.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(83)))), ((int)(((byte)(33)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(248, 100);
            this.button1.TabIndex = 0;
            this.button1.Text = "Exit";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(83)))), ((int)(((byte)(33)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(248, 127);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 71);
            this.label1.TabIndex = 1;
            this.label1.Text = "Draughts";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lineStartWithWhite4);
            this.panel4.Controls.Add(this.lineStartWithBlack4);
            this.panel4.Controls.Add(this.lineStartWithWhite3);
            this.panel4.Controls.Add(this.lineStartWithBlack3);
            this.panel4.Controls.Add(this.lineStartWithWhite2);
            this.panel4.Controls.Add(this.lineStartWithBlack2);
            this.panel4.Controls.Add(this.lineStartWithWhite1);
            this.panel4.Controls.Add(this.lineStartWithBlack1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(250, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 800);
            this.panel4.TabIndex = 1;
            // 
            // lineStartWithWhite4
            // 
            this.lineStartWithWhite4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lineStartWithWhite4.Location = new System.Drawing.Point(0, 0);
            this.lineStartWithWhite4.Name = "lineStartWithWhite4";
            this.lineStartWithWhite4.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithWhite4.TabIndex = 7;
            // 
            // lineStartWithBlack4
            // 
            this.lineStartWithBlack4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithBlack4.Location = new System.Drawing.Point(0, 100);
            this.lineStartWithBlack4.Name = "lineStartWithBlack4";
            this.lineStartWithBlack4.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithBlack4.TabIndex = 6;
            // 
            // lineStartWithWhite3
            // 
            this.lineStartWithWhite3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithWhite3.Location = new System.Drawing.Point(0, 200);
            this.lineStartWithWhite3.Name = "lineStartWithWhite3";
            this.lineStartWithWhite3.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithWhite3.TabIndex = 5;
            // 
            // lineStartWithBlack3
            // 
            this.lineStartWithBlack3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithBlack3.Location = new System.Drawing.Point(0, 300);
            this.lineStartWithBlack3.Name = "lineStartWithBlack3";
            this.lineStartWithBlack3.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithBlack3.TabIndex = 4;
            // 
            // lineStartWithWhite2
            // 
            this.lineStartWithWhite2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithWhite2.Location = new System.Drawing.Point(0, 400);
            this.lineStartWithWhite2.Name = "lineStartWithWhite2";
            this.lineStartWithWhite2.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithWhite2.TabIndex = 3;
            // 
            // lineStartWithBlack2
            // 
            this.lineStartWithBlack2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithBlack2.Location = new System.Drawing.Point(0, 500);
            this.lineStartWithBlack2.Name = "lineStartWithBlack2";
            this.lineStartWithBlack2.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithBlack2.TabIndex = 2;
            // 
            // lineStartWithWhite1
            // 
            this.lineStartWithWhite1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithWhite1.Location = new System.Drawing.Point(0, 600);
            this.lineStartWithWhite1.Name = "lineStartWithWhite1";
            this.lineStartWithWhite1.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithWhite1.TabIndex = 1;
            // 
            // lineStartWithBlack1
            // 
            this.lineStartWithBlack1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lineStartWithBlack1.Location = new System.Drawing.Point(0, 700);
            this.lineStartWithBlack1.Name = "lineStartWithBlack1";
            this.lineStartWithBlack1.Size = new System.Drawing.Size(800, 100);
            this.lineStartWithBlack1.TabIndex = 0;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1050, 800);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Draughts";
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private LineStartWithWhite lineStartWithWhite4;
        private LineStartWithBlack lineStartWithBlack4;
        private LineStartWithWhite lineStartWithWhite3;
        private LineStartWithBlack lineStartWithBlack3;
        private LineStartWithWhite lineStartWithWhite2;
        private LineStartWithBlack lineStartWithBlack2;
        private LineStartWithWhite lineStartWithWhite1;
        private LineStartWithBlack lineStartWithBlack1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

